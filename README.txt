
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration 


INTRODUCTION
------------

Current Maintainer: Thijs Simonis <mail@thijssimonis.nl>

This module provides Drupal administrators to send a email reminder
to a unactivated user automatically and remove it automatically.


INSTALLATION
------------

To install this module you need the following modules

* Token, http://drupal.org/project/token


CONFIGURATION
-------------

You can find configuration page for this module on 

* [Administer > Configuration > People > Unactivated user]
* admin/config/people/unactivated-users
