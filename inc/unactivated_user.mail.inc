<?php
/**
 * @file
 * The unactivated_user module automaticly handling unactivated users.
 */

function _unactivated_user_mail($token, $user = NULL) {
  $user = ($user) ? $user : $GLOBALS['user'];

  switch ($token) {
    case 'user-reminder':
      $subject = token_replace(variable_get('unactivated_user_mail_reminder_subject', '[site:name]: Unactivated account'), array('user' => $user));
      $body = token_replace(variable_get('unactivated_user_mail_reminder_body', 'Your message here!'), array('user' => $user));
      $to = $user->mail;
      break;
  }

  if (!empty($to)) {
    $from = variable_get('site_mail', 'no-reply@example.com');
    $message = array(
      'id' => 'unactivated_user_' . $token,
      'to' => $to,
      'subject' => $subject,
      'body' => array($body),
      'headers' => array(
        'From' => variable_get('site_name', 'Example') . ' <' . $from . '>',
        'Sender' => $from,
        'Return-Path' => $from,
      ),
    );
    $system = drupal_mail_system('unactivated_user', $token);
    $message = $system->format($message);

    return ($system->mail($message)) ? TRUE : FALSE;
  }
  else {
    return TRUE;
  }
}