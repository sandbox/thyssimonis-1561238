<?php
/**
 * @file
 * The unactivated_user module automaticly handling unactivated users.
 */

/**
 * Implements hook_form().
 */
function unactivated_user_settings_form($form, &$form_state) {
  // Reminde/delete settings.
  $form['days'] = array(
    '#type' => 'fieldset',
    '#title' => t('Days'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['days']['unactivated_user_reminder_days'] = array(
    '#title' => t('Reminder days'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => variable_get('unactivated_user_reminder_days', 15),
    '#required' => TRUE,
    '#description' => t('The number of days when a reminder mail will be sent.'),
  );
  $form['days']['unactivated_user_delete_days'] = array(
    '#title' => t('Delete days'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => variable_get('unactivated_user_delete_days', 30),
    '#required' => TRUE,
    '#description' => t('The number of days when a user will be deleted.'),
  );

  // Mail settings.
  $form['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['mail']['unactivated_user_mail_reminder_subject'] = array(
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#default_value' => variable_get('unactivated_user_mail_reminder_subject', '[site:name]: Unactivated account'),
    '#required' => TRUE,
  );
  $form['mail']['unactivated_user_mail_reminder_body'] = array(
    '#title' => t('Body'),
    '#type' => 'textarea',
    '#rows' => '20',
    '#default_value' => variable_get('unactivated_user_mail_reminder_body', 'Your message here!'),
    '#required' => TRUE,
    '#description' => t('You can use patterns.'),
  );

  // Paterns.
  $form['mail']['patterns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['mail']['patterns']['tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('unactivated_user', 'user'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_validate().
 */
function unactivated_user_settings_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['unactivated_user_reminder_days'])) {
    form_set_error('unactivated_user_reminder_days', t('Field: Reminder days is not numeric.'));
  }
  if (!is_numeric($form_state['values']['unactivated_user_delete_days'])) {
    form_set_error('unactivated_user_delete_days', t('Field: Delete days is not numeric.'));
  }
}
